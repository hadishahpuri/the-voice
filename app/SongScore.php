<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongScore extends Model
{
    protected $guarded = [];

    protected $table = 'song_scores';
}
