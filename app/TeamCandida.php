<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamCandida extends Model
{

    protected $guarded = [];

    protected $table = 'team_candidates';
}
