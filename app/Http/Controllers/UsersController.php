<?php

namespace App\Http\Controllers;

use App\Song;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function Login()
    {
        return view('layouts.login');
    }


    public function Authenticate(Request $request)
    {
        $v = Validator::make([
            'email' => $request->email,
            'password' => $request->password
        ],[
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);
        $errorMessage = implode("<br />", $v->messages()->all());
        if ($v->fails()) {
            $contents = "ShowMessage('error','" . $errorMessage . "',false,false);";
            $contents .= "HideLoader();";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'row_status' => 'active', 'type' => ['admin', 'mentor']], true)) {
            $contents = "window.location='" . url(route("ShowHomePage")) . "';";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;

        }else{
            $contents = "ShowMessage('error','emil or password incorrect',false,false);";
            $contents .= "HideLoader()";
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        }
    }

    public function HomePageData(Request $request)
    {
        $user = Auth::user();
        $teams = [];
        $songs = [];
        if ($user->type == 'admin') {
            $teams = Team::join('users', 'users.id', '=', 'teams.mentor_id')
                                            ->select('teams.*', 'users.name as mentor_name')
                                            ->where('teams.row_status', 'active')
                                            ->get();

            $songs = Song::join('users', 'users.id', '=', 'songs.candida_id')
                                        ->select('songs.*', 'users.name as candida_name')
                                        ->where('songs.row_status', 'active')
                                        ->get();
        }else {
            $teams = Team::where('row_status', 'active')
                                            ->where('mentor_id', $user->id)
                                            ->get()
                                            ->toArray();

            if (! empty($teams)) {
                $teamIds = Arr::pluck($teams, 'id');

                $songs = User::join('team_candidates', 'team_candidates.candida_id', '=', 'users.id')
                                            ->join('songs', 'songs.candida_id', '=', 'users.id')
                                            ->select('songs.*', 'users.name as candida_name')
                                            ->where('songs.row_status', 'active')
                                            ->whereIn('team_candidates.team_id', $teamIds)
                                            ->get()
                                            ->toArray();
            }
        }

        return [
            'msg' => 'success',
            'teams' => $teams,
            'songs' => $songs
        ];
    }


}
