<?php

namespace App\Http\Controllers;

use App\Song;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TeamsController extends Controller
{
    public function TeamsIndex(Request $request)
    {
        if (Auth::user()->type != 'admin') {
            return [
                'msg' => 'error',
                'error' => 'permission denied'
            ];
        }

        $teams = Team::where('row_status', 'active')
                                        ->get();

        return [
            'msg' => 'success',
            'teams' => $teams
        ];
    }


    public function Profile($id)
    {
        $candida = User::join('team_candidates', 'team_candidates.candida_id', '=', 'users.id')
                                        ->select('users.*', 'team_candidates.team_id')
                                        ->where('users.id', $id)
                                        ->where('team_candidates.row_status', 'active')
                                        ->first();

        $songs = Song::where('candida_id', $candida->id)
                                        ->get()
                                        ->toArray();
        $songsTotalScore = 0;
        foreach ($songs as $song) {
            $songsTotalScore += $song['average_score'];
        }

        $songsAverage = $songsTotalScore / count($songs);

        $teamScores = Song::join('team_candidates', 'team_candidates.candida_id', '=', 'songs.candida_id')
                                                ->select('songs.average_score')
                                                ->where('team_candidates.team_id', $candida->team_id)
                                                ->where('songs.row_status', 'active')
                                                ->get()
                                                ->toArray();


        $teamTotalScore = 0;
        foreach ($teamScores as $score) {;
            $teamTotalScore += $score['average_score'];
        }

        $teamAverage = $teamTotalScore / count($teamScores);

        return view('profile', compact('candida', 'songs', 'songsAverage', 'teamAverage'));
    }
}
