<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SongsController extends Controller
{
    public function GetSongs(Request $request)
    {
        $songs = Song::where('songs.row_status', 'active');
        if ($request->team_id > 0) {
            $songs = $songs->join('team_candidates', 'team_candidates.candida_id', '=', 'songs.candida_id')
                                                ->where('team_candidates.team_id', $request->team_id);
        }else {
            if (Auth::user()->type == 'mentor') {
                $songs = $songs->join('team_candidates', 'team_candidates.candida_id', '=', 'songs.candida_id')
                                                ->where('team_candidates.mentor_id', Auth::user()->id);
            }
        }

        $songs = $songs->join('users', 'users.id', '=', 'songs.candida_id')
                                        ->select('songs.*', 'users.name as candida_name')
                                        ->orderBy('songs.last_update_ts', 'DESC')
                                        ->get();

        return [
            'msg' => 'success',
            'songs' => $songs
        ];
    }
}
