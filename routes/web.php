<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'UsersController@Login')->name('Login');
Route::get('/logout', 'UsersController@Login')->name('Logout');
Route::post('/authenticate', 'UsersController@Authenticate')->name('Authenticate');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('welcome');
    })->name('ShowHomePage');

    Route::post('/home/data', 'UsersController@HomePageData')->name('HomePageData');
    Route::get('/profile/{user}', 'TeamsController@Profile')->name('Profile');
    Route::post('/songs_data', 'SongsController@GetSongs')->name('GetSongs');
    Route::post('/teams}', 'TeamsController@TeamsIndex')->name('TeamsIndex');
});
