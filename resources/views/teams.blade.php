@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Teams</h1>
        <div class="row">
            <div class="col-sm-12">
                <h3 class="text-dark mb-2">Teams</h3>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Mentor</th>
                            <th scope="col">Average Score</th>
                        </tr>
                        </thead>
                        <tbody id="tblBody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('newScript')
    <script>
        $(document).ready(function () {

        })
    </script>
@endpush
