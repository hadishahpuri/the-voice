@extends('layouts.app')

@section('content')
        <div class="container">
            <h1>
                The Voice
            </h1>
            <p>{{ $candida->name }} profile</p>
            <hr>
            <div class="row">
                <div class="col-sm-12">Candida Average Score: {{ $songsAverage }}</div>
                <div class="col-sm-12">Team Average Score: {{ $teamAverage }}</div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="text-dark mb-2">{{ $candida->name }} Songs</h3>
                    <div class="table-responsive">
                        <table class="table align-items-center table-dark table-flush">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Candida</th>
                                <th scope="col">Performance Date</th>
                                <th scope="col">Average Score</th>
                            </tr>
                            </thead>
                            <tbody id="tblBody">
                            @if(empty($songs))
                                <tr>
                                    <td>No Songs Available</td>
                                </tr>
                            @else
                                @foreach($songs as $song)
                                    <tr data-field-song-id="{{ $song['id'] }}">
                                        <td>
                                            {{ $song['name'] }}
                                        </td>
                                        <td>
                                            {{ $candida['name'] }}
                                        </td>
                                        <td>
                                            {{ $song['performance_date'] }}
                                        </td>
                                        <td>
                                            {{ $song['average_score'] }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
