<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('js/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/theVoice.css') }}">
    @stack('newStyle')
</head>
<body>

    @yield('content')


    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
                <div class="object"></div>
            </div>
        </div>
    </div>
    <div class="modal_box">
        <div class="innerbox">
            <div class="exit"></div>
            <div class="main_text">
                <div class="title">popup title</div>
                <div class="mohtava" id="mohtava">
                    <div id="box_loading">
                        <div id="loading-center">
                            <div id="loading-center-absolute">
                                <div class="box_object" id="box_object_one"></div>
                                <div class="box_object" id="box_object_two"></div>
                                <div class="box_object" id="box_object_three"></div>
                                <div class="box_object" id="box_object_four"></div>
                                <div class="box_object" id="box_object_five"></div>
                                <div class="box_object" id="box_object_six"></div>
                                <div class="box_object" id="box_object_seven"></div>
                                <div class="box_object" id="box_object_eight"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="msgBox">
        <div class="innerbox">
            <div class="exit"></div>
            <div class="topSide"></div>
            <div class="bottomSide"></div>
            <div class="btns">
                <div class="btn confirmBtn">accept</div>
                <div class="btn cancelBtn">reject</div>
            </div>
        </div>
    </div>


<script src="{{ asset('js/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery/jquery.form.js') }}"></script>
<script src="{{ asset('js/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/theVoice.js') }}"></script>
@stack('newScript')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</body>
</html>
