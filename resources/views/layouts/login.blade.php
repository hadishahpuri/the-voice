@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>
            Login
        </h1>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <form method="POST" name="LoginFrm" id="LoginFrm">
                    <div class="col-sm-12">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                    <div class="col-sm-12">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <input type="button" class="btn btn-info btn-block" style="margin-top: 20px;" id="loginBtn" value="Login">
                </form>
            </div>
        </div>
    </div>
@endsection

@push('newScript')
    <script>
        $('#loginBtn').click(function () {
            ShowLoader()
            CallAjax('{{ route('Authenticate') }}', {email: $('#email').val(), password: $('#password').val()})
        })
    </script>
@endpush
