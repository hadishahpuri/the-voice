@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>
            The Voice
        </h1>
        <hr>
        <div class="row">
            <div class="col-sm-12">
            @if(Auth::user()->type == 'admin')
                <ul class="menu">
                    <li class="menuItem">
                        <a href="#" id="teams">Teams</a>
                    </li>
                    <li class="menuItem">
                        <a href="#">Candidates</a>
                    </li>
                    <li class="menuItem">
                        <a href="{{ route('Logout') }}">Logout</a>
                    </li>
                </ul>
                <hr>
                <div class="row" dir="rtl">
                    <div class="col-md-3">
                        <h2>filters</h2>
                        <select name="teams" id="teamsFilter">
                            <option value="-1">All</option>
                        </select>
                    </div>
                </div>
                @else
                    <ul class="menu">
                        <li class="logout">
                            <a href="{{ route('Logout') }}">Logout</a>
                        </li>
                    </ul>
                    <hr>
            @endif
                <h3 class="text-dark mb-2" id="tableHeader">Songs</h3>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Candida</th>
                            <th scope="col">Performance Date</th>
                            <th scope="col">Average Score</th>
                        </tr>
                        </thead>
                        <tbody id="tblBody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('newScript')
    <script>
        $(document).ready(function () {
            CallAjaxFunc('{{ route('HomePageData') }}', {}, HomeDataSuccess)

            function HomeDataSuccess(res) {
                res.teams.forEach(team => {
                    $('#teamsFilter').append(`
                        <option value="${team.id}">${team.name}</option>
                    `)
                })

                if (res.songs.length > 0) {
                    res.songs.forEach(song => {
                        $('#tblBody').append(`
                            <tr data-field-song-id="${song.id}">
                                <td>
                                    ${song.name}
                                </td>
                                <td>
                                    <a href="/profile/${song.candida_id}">${song.candida_name}</a>
                                </td>
                                <td>
                                    ${song.performance_date}
                                </td>
                                <td>
                                    ${song.average_score}
                                </td>
                            </tr>
                        `)
                    })
                }else {
                    $('#tblBody').append(`
                        <tr>
                            <td>No Songs Available</td>
                        </tr>
                    `)
                }
            }

            $('#teamsFilter').change(function () {
                console.log($('#teamsFilter').val());
                ShowLoader();
                CallAjaxFunc('{{ route('GetSongs') }}', {team_id: $('#teamsFilter').val()}, GetSongsSuccess)
            })

            function GetSongsSuccess(res) {
                HideLoader();
                $('#tblBody').empty()
                if (res.songs.length > 0) {
                    res.songs.forEach(song => {
                        $('#tblBody').append(`
                            <tr data-field-song-id="${song.id}">
                                <td>
                                    ${song.name}
                                </td>
                                <td>
                                    <a href="/profile/${song.candida_id}">${song.candida_name}</a>
                                </td>
                                <td>
                                    ${song.performance_date}
                                </td>
                                <td>
                                    ${song.average_score}
                                </td>
                            </tr>
                        `)
                    })
                }else {
                    $('#tblBody').append(`
                        <tr>
                            <td>No Songs Available</td>
                        </tr>
                    `)
                }
            }
        })
    </script>
@endpush
