
$(document).ready(function() {
	$(".replyBox .back").click(function(){
		$(".replyBox").fadeOut();
    });
    $(".modal_box .exit").click(function(){
		$(".modal_box").fadeOut();
	});

	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$(".replyBox").fadeOut();
            $(".msgTxt").fadeOut();
            $(".modal_box").fadeOut();
		}
	});
	$(".replyBox").click(function(e) {
	    if (e.target === this) {
	        $(".replyBox").fadeOut();
	    }
    });
    $(".modal_box").click(function(e) {
	    if (e.target === this) {
	        $(".modal_box").fadeOut();
	    }
	});
	$(".msgTxt").click(function(e) {
	    if (e.target === this) {
	        $(".msgTxt").fadeOut();
	    }
	});

	$(".changepwBox").click(function(e) {
	    if (e.target === this) {
	        $(".changepwBox").fadeOut();
	    }
	});

	$(".changepwBox .exit").click(function(){
		$(".changepwBox").fadeOut();
	});

	$(".msgTxt .exit").click(function(){
		$(".msgTxt").fadeOut();
	});

	$(".userInfo .exit").click(function(){
		$(".userInfo").fadeOut();
	});
	$(".userInfo").click(function(e) {
	    if (e.target === this) {
	        $(".userInfo").fadeOut();
	    }
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$(".userInfo").fadeOut();
		}
	});
});

function ShowMessage(type,msg,btns,reload,confirmBtnfunc) {

    $('.msgBox .bottomSide').html(msg);

    if (btns==false)
		$('.msgBox .btns').hide();
	else{
		$('.msgBox .btns').show();
		$('.msgBox .confirmBtn').on("click", confirmBtnfunc);
		//$(".btn.confirmBtn").attr("id",confirmBtnId)
	}

    if(type!='error')
        $('.msgBox').addClass('success');
    else
        $('.msgBox').removeClass('success');

    $('.msgBox').fadeIn();

    if (reload==true) {
        $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".msgBox").fadeOut();
            location.reload();
        }
        });
        $('.msgBox .exit , .msgBox .cancelBtn').click(function() {
            $('.msgBox').fadeOut();
            location.reload();
        });
        $('.msgBox').click(function(e) {
            if (e.target === this) {
                $('.msgBox').fadeOut();
                location.reload();
            };
        });
    } else if(reload==false) {
        $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".msgBox").fadeOut();
        }
        });
        $('.msgBox .exit , .msgBox .cancelBtn').click(function() {
            $('.msgBox').fadeOut();
        });
        $('.msgBox').click(function(e) {
            if (e.target === this) {
                $('.msgBox').fadeOut();
            };
        });
    } else {
        $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".msgBox").fadeOut();
            window.location.href = APP_URL+reload;
        }
        });
        $('.msgBox .exit , .msgBox .cancelBtn').click(function() {
            $('.msgBox').fadeOut();
            window.location.href = APP_URL+reload;
        });
        $('.msgBox').click(function(e) {
            if (e.target === this) {
                $('.msgBox').fadeOut();
                window.location.href = APP_URL+reload;
            };
        });
    }
}

function ShowLoader() {
    $('#loading').fadeIn();
}
function HideLoader() {
    $('#loading').fadeOut();
}


function CallAjax(ajaxurl,ajaxdata,pnlres) {
    $.ajax({
        method: "POST",
        url: ajaxurl,
        data: ajaxdata,
        statusCode: {
            404: function() {
                ShowMessage('error','خطا در ارسال اطلاعات.<br />لطفا اتصال اینترنت خود را بررسی کنید.','false','false');
            }
        }
        ,
        success:function(data) {
            if(pnlres!='')
                $('#'+pnlres).html(data)
        }
    });
}

function CallAjaxFunc(ajaxurl,ajaxdata,func) {
    $.ajax({
        method: "POST",
        url: ajaxurl,
        data: ajaxdata,
        statusCode: {
            404: function() {
                ShowMessage('error','خطا در ارسال اطلاعات.<br />لطفا اتصال اینترنت خود را بررسی کنید.','false','false');
            }
        }
        ,
        success:func
    });
}

function ShowPopUp(title,url,parametr,width,height) {
	$('.modal_box .innerbox').css('max-width',width);
	$('.modal_box .innerbox').css('min-height',height);
	$(".main_text .title").html(title);
	$(".modal_box").show();
	CallAjax(url,parametr,"mohtava");
}

function ShowText(title,text) {
	$(".main_text .title").html(title);
	$("#mohtava").html(text);
	$(".modal_box").show();
}

function DefaultImg(obj) {
    obj.src = 'img/profiles/default profile.png';
}


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(input).siblings('.previewPic').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
