<?php

use Illuminate\Database\Seeder;

class SongSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $candidates =  \App\User::where('type', 'candida')
                                                            ->get()
                                                            ->toArray();
        $mentors = \App\User::where('type', 'mentor')
                                                    ->get()
                                                    ->toArray();
        for ($i = 0; $i < count($candidates); $i++) {
            $song = new \App\Song();
            $song->name = "song_$i";
            $song->candida_id = $candidates[$i]['id'];
            $song->performance_date = \Carbon\Carbon::now();
            $song->average_score = 0;
            $song->row_status = 'active';
            $song->last_update_ts = MiliTime();
            $song->save();


            for ($j = 0; $j < count($mentors); $j++) {
                $songScore = new \App\SongScore();
                $songScore->song_id = $song->id;
                $songScore->mentor_id = $mentors[$j]['id'];
                $songScore->score = rand(0, 100);
                $songScore->row_status = 'active';
                $songScore->last_update_ts = MiliTime();
                $songScore->save();
            }

            $scores = \App\SongScore::where('song_id', $song->id)
                                                                ->get();
            $scoresArray = \Illuminate\Support\Arr::pluck($scores, 'score');
            $totalScore = 0;
            foreach ($scoresArray as $score) {
                $totalScore += $score;
            }
            $countScores = count($scores);
            $averageScore = $totalScore / doubleval($countScores);
            \App\Song::where('id', $song->id)
                                ->update([
                                    'average_score' => $averageScore
                                ]);
        }
    }
}
