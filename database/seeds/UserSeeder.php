<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 40; $i++) {
            $user = new \App\User();
            $user->name = "candida_$i";
            $user->email = "candida_$i" . '@gmail.com';
            $user->type = 'candida';
            $user->password = \Illuminate\Support\Facades\Hash::make('candida');
            $user->row_status = 'active';
            $user->last_update_ts = MiliTime();
            $user->save();
        }


        for ($i = 0; $i < 4; $i++) {
            $user = new \App\User();
            $user->name = "mentor_$i";
            $user->email = "mentor_$i" . '@gmail.com';
            $user->type = 'mentor';
            $user->password = \Illuminate\Support\Facades\Hash::make('mentor');
            $user->row_status = 'active';
            $user->last_update_ts = MiliTime();
            $user->save();
        }

        $user = new \App\User();
        $user->name = 'admin';
        $user->email = 'admin@gmail.com';
        $user->type = 'admin';
        $user->password = \Illuminate\Support\Facades\Hash::make('admin');
        $user->row_status = 'active';
        $user->last_update_ts = MiliTime();
        $user->save();
    }
}
