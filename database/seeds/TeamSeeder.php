<?php

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mentors = \App\User::where('type', 'mentor')
                                                    ->get()
                                                    ->toArray();

        for ($i = 0; $i < count($mentors); $i++) {
            $teamA = new \App\Team();
            $teamA->name = "team_$i";
            $teamA->mentor_id = $mentors[$i]['id'];
            $teamA->row_status = 'active';
            $teamA->last_update_ts = MiliTime();
            $teamA->save();

            $n = $i * 10 +1;
            for ($j = $n; $j < $n + 10; $j++) {
                $teamCandida = new \App\TeamCandida();
                $teamCandida->team_id = $teamA->id;
                $teamCandida->candida_id = $j;
                $teamCandida->mentor_id = $teamA->mentor_id;
                $teamCandida->row_status = 'active';
                $teamCandida->last_update_ts = MiliTime();
                $teamCandida->save();
            }
        }
    }
}
