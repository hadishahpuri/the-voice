<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSongScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_scores', function (Blueprint $table) {
            $table->id();
            $table->integer('song_id');
            $table->integer('mentor_id');
            $table->integer('score');
            $table->enum('row_status', ['active', 'deleted', 'updated', 'suspended']);
            $table->bigInteger('last_update_ts')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('song_scores');
    }
}
