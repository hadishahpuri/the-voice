<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_candidates', function (Blueprint $table) {
            $table->id();
            $table->integer('team_id');
            $table->integer('candida_id');
            $table->integer('mentor_id');
            $table->enum('row_status', ['active', 'deleted','updated', 'suspended']);
            $table->bigInteger('last_update_ts')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_candidates');
    }
}
